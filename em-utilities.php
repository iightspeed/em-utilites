<?php
/**
    Plugin Name: EM Utilities
    Plugin URI: http://*.com/
    Description: EM toolbox.
    Version: 1.1.16
    Author: Excite Media
    Author URI: http://www.excitemedia.com.au
    License: EMWPDPHPE
    Bitbucket Plugin URI: https://iightspeed@bitbucket.org/iightspeed/em-utilites.git
**/

include ('modules/wp_vulnerability_scan.php' );

if(isset($_GET['task'])){
    $task = $_GET['task'];

    if($task == "performScanNow"){
        em_wp_vulnerability_scan_shedule_once();
        echo 'Scan Complete';
        exit();
    }
}

$em_php_editing_setting = 'test';

/** ACTIVATION **/

register_activation_hook(__FILE__, 'em_utilites_activation');

function em_utilites_activation() {
    em_wp_vulnerability_scan_shedule();
    em_wp_vulnerability_scan_shedule_once();
}

/** DEACTIVATION **/
register_deactivation_hook(__FILE__, 'my_deactivation');

function my_deactivation() {
    wp_clear_scheduled_hook('em_wp_vulnerability_scan');
    wp_clear_scheduled_hook('em_wp_vulnerability_scan()');
    wp_clear_scheduled_hook('em_wp_vulnerability_scan_hook');
}


function wpse27856_set_content_type(){
    return "text/html";
}
add_filter( 'wp_mail_content_type','wpse27856_set_content_type' );

function enqueue_disable_php_script( $hook ) {

    global $em_php_editing_setting;

    if(esc_attr(get_option('disabled-extent'))){
    	$em_php_editing_setting = esc_attr(get_option('disabled-extent'));
    }else{
    	$em_php_editing_setting = 'functions';
    }

    if($em_php_editing_setting == 'all'){
    	define( 'DISALLOW_FILE_EDIT', true );
    }else if($em_php_editing_setting == 'php'){
    	if ('theme-editor.php' != $hook && 'plugin-editor.php' != $hook) {
        	return;
    	}
    	wp_enqueue_script( 'hide_php', plugin_dir_url( __FILE__ ) . 'library/js/hide-php.js' );
    }else if($em_php_editing_setting == 'functions'){
    	if ('theme-editor.php' != $hook && 'plugin-editor.php' != $hook) {
        	return;
    	}
    	wp_enqueue_script( 'hide_php', plugin_dir_url( __FILE__ ) . 'library/js/hide-functions.js' );
    }


}

add_action('admin_enqueue_scripts', 'enqueue_disable_php_script');

// create settings menu
add_action('admin_menu', 'create_my_menu');
function create_my_menu() {
	//create new top-level menu
	add_submenu_page('options-general.php', 'EM Utilities - Settings', 'EM Utilities', 'administrator', 'em_settings', 'em_settings_page');
	//call register settings function
	add_action( 'admin_init', 'register_em_settings' );
}
function register_em_settings() {
	//register our settings
	register_setting( 'em-disable-php-editing-group', 'disabled-extent' );
	register_setting( 'em-disable-php-editing-group', 'vul-ignore' );
    register_setting( 'em-disable-php-editing-group', 'do-vul-scan' );
    register_setting( 'em-disable-php-editing-group', 'show-custom-login' );
}
function em_settings_page() {
?>

<link rel="stylesheet" type="text/css" href="<?php echo(plugin_dir_url( __FILE__ )); ?>library/css/em-styles.css">

<div class="wrap">
<br/>
<h2></h2>
<h2>EM Utilities - Settings</h2>

<div id="em-settings-tabs-container">

    <div class="em-settings-tab active" id="em-settings-general-tab" >
        <div class="em-settings-title">
            General
        </div>
    </div>
    <?php if(esc_attr(get_option('do-vul-scan')) == 'true'){ ?>
    <div class="em-settings-tab" id="em-settings-vulscan-tab">
        <div class="em-settings-title">
            Vulnerability Scan
        </div>
    </div>
    <?php } ?>
</div>

<form method="post" action="options.php">
    <?php settings_fields( 'em-disable-php-editing-group' ); ?>
    <?php do_settings_sections( 'em-disable-php-editing-group' ); ?>

<div id="em-settings-tab-content-container">
    <div class="em-settings-tab-content active" id="em-settings-general-content">
            <h2>General Settings</h2>
            <div>
                <input type="checkbox" name="show-custom-login" value="true" <?php if(esc_attr(get_option('show-custom-login')) == 'true'){echo('checked');} ?>>Show Custom Login<br>
                <input type="checkbox" name="do-vul-scan" value="true" <?php if(esc_attr(get_option('do-vul-scan')) == 'true'){echo('checked');} ?>>Enable Vulnerability Scan Module<br>
            </div>
            <hr/>
            <div>
                <?php global $em_php_editing_setting;?>
                <strong>Disable PHP Editing</strong><br/>
                <input type="radio" name="disabled-extent" value="all" <?php if($em_php_editing_setting == 'all'){echo('checked');} ?>> All editing<br>
            <input type="radio" name="disabled-extent" value="php" <?php if($em_php_editing_setting == 'php'){echo('checked');} ?>> All PHP<br>
            <input type="radio" name="disabled-extent" value="functions" <?php if($em_php_editing_setting == 'functions'){echo('checked');} ?>> Only functions.php
            </div>
    </div>
    <div class="em-settings-tab-content" id="em-settings-vulscan-content">
            <h2>Vulnerability Scan Settings</h2>
            <div>
                <button class="button-primary" id="em_vs_button">Do Vulnerability Scan Now</button>
            </div>
            <div>
                ignore vulnerabilities (comma seperated).
            </div>
            <div>
                <textarea name="vul-ignore"><?php echo esc_attr( get_option('vul-ignore') ); ?></textarea>
            </div>
    </div>
</div>
    
    <?php submit_button(); ?>

</form>


</div>
<script>
    jQuery(document).ready(function(){
        jQuery('#em_vs_button').click(function(event) {
            //alert('<?php echo(plugin_dir_url( __FILE__ )); ?>/modules/wp_vulnerability_scan.php?task=performScanNow');
            event.preventDefault();
             jQuery.ajax({
              type: "POST",
              url: "<?php echo(plugin_dir_url( __FILE__ )); ?>/em-utilites.php?task=performScanNow"
            }).done(function( msg ) {
              alert( msg );
            });    

        });

        jQuery('.em-settings-tab').click(function(){
            jQuery('.em-settings-tab.active').removeClass('active');
            jQuery('.em-settings-tab-content.active').removeClass('active');
            jQuery(this).addClass('active');

            var tabName = jQuery(this).attr('id');
            tabName = tabName.replace("-tab","-content");

            jQuery("#"+tabName).addClass('active');

            
        });
    });
</script>
<?php
} 

// Custom WP Admin Logo
if(esc_attr(get_option('show-custom-login')) == 'true'){
    add_action('login_head', 'em_utilities_custom_login_logo');
}

function em_utilities_custom_login_logo() {
    echo '<style type="text/css">   
    body{background-color:#111;}    
    .login #backtoblog a, .login #nav a{color:#fff;}    
    .login #backtoblog a:hover, 
    .login #nav a:hover{color:#46dbae;} 
    h1 a { background-image:url('.plugin_dir_url( __FILE__ ).'library/images/wp-logo.png) !important; width:auto!important; background-size:auto!important;height: 54px !important;margin: 0 !important; }    
    </style>';
}



?>