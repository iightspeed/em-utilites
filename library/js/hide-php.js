jQuery(document).ready(function(){

	jQuery('#templateside li').each(function(){
		if (jQuery(this).text().indexOf(".php") >= 0){
			jQuery(this).css('opacity','0.3');
		}
	});



	if (jQuery('.fileedit-sub').text().indexOf(".php") >= 0){
		jQuery('.fileedit-sub').append('<p>Editing this file has been disabled for security.</p> <p>A syntax error in this file can cause your wordpress install to not run at all.</p> <p> Please connect via FTP to edit this file.</p>');
		jQuery('#newcontent').hide();
		jQuery('.submit').hide();
	}
});
